When /^I visit "(.*)\s*"$/ do |uri|
  visit "#{uri}"
  sleep 0.5
end

When(/^I visit the home page$/) do
  visit "/"
  sleep 0.5
end

Then /^I should( not)? see "([^"]*)"\s*$/ do |negative, text|
  if negative then
    page.should_not have_xpath(".//*[contains(text(),\"#{text}\")]", :visible => true)
  else
    page.should have_content(text)
  end
end

When /^I fill in "([^"]*)" with "([^"]*)"$/ do |page_el, arg|
  fill_in page_el, :with => arg
end

Then /^(?:I )?(?:click|click on) "([^"]*)"\s*$/ do |the_link|
  sleeping(2).seconds.between_tries.failing_after(5).tries do
    click_link_or_button(the_link)
  end
end

Then /^I pause "([^"]*)" seconds$/ do |tm_sec|
  puts ">>> sleeping #{tm_sec} <<<"
  sleep tm_sec.to_i
end
