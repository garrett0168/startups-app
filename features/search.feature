@javascript
Feature:  Search a startup
        
    To search for a startup
    As a user
    I need to submit a domain name

Scenario: As a user, I can search for a startup by domain name
    When I visit the home page
    And I fill in "domain-input" with "visible.vc"
    When I click "Submit"
    And I pause "2" seconds
    Then I should see "Visible"
    And I should see "Data sharing for startups and their investors."

Scenario: As a user, I can be notified if the startup cannot be found
    When I visit the home page
    And I fill in "domain-input" with "asdf"
    When I click "Submit"
    And I pause "2" seconds
    Then I should see "We were unable to find any information. Please make sure you typed the domain correctly."

