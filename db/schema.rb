# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131014191349) do

  create_table "locations", force: true do |t|
    t.integer "angellist_location_id"
    t.string  "tag_type"
    t.string  "display_name"
    t.string  "angellist_url"
  end

  create_table "locations_startups", force: true do |t|
    t.integer "startup_id"
    t.integer "location_id"
  end

  add_index "locations_startups", ["location_id"], name: "index_locations_startups_on_location_id", using: :btree
  add_index "locations_startups", ["startup_id"], name: "index_locations_startups_on_startup_id", using: :btree

  create_table "markets", force: true do |t|
    t.integer "angellist_market_id"
    t.string  "tag_type"
    t.string  "display_name"
    t.string  "angellist_url"
  end

  add_index "markets", ["angellist_market_id"], name: "index_markets_on_angellist_market_id", using: :btree

  create_table "markets_startups", force: true do |t|
    t.integer "startup_id"
    t.integer "market_id"
  end

  add_index "markets_startups", ["market_id"], name: "index_markets_startups_on_market_id", using: :btree
  add_index "markets_startups", ["startup_id"], name: "index_markets_startups_on_startup_id", using: :btree

  create_table "screenshots", force: true do |t|
    t.integer "startup_id"
    t.string  "thumb"
    t.string  "original"
  end

  add_index "screenshots", ["startup_id"], name: "index_screenshots_on_startup_id", using: :btree

  create_table "startups", force: true do |t|
    t.integer "angellist_id",   null: false
    t.string  "name",           null: false
    t.string  "domain",         null: false
    t.text    "description"
    t.text    "high_concept"
    t.string  "angellist_url",  null: false
    t.string  "logo_url"
    t.string  "thumb_url"
    t.string  "company_url"
    t.string  "crunchbase_url"
    t.string  "blog_url"
    t.string  "video_url"
    t.string  "twitter_url"
    t.text    "status"
    t.integer "quality"
    t.integer "follower_count"
  end

  add_index "startups", ["angellist_id"], name: "index_startups_on_angellist_id", using: :btree

end
