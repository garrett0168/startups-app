class CreateStartups < ActiveRecord::Migration
  def change
    create_table :startups do |t|
      t.integer :angellist_id, null: false
      t.string :name, null: false
      t.string :domain, null: false
      t.text :description
      t.text :high_concept
      t.string :angellist_url, null: false
      t.string :logo_url
      t.string :thumb_url
      t.string :company_url
      t.string :crunchbase_url
      t.string :blog_url
      t.string :video_url
      t.string :twitter_url
      t.text :status
      t.integer :quality
      t.integer :follower_count
    end

    create_table :markets do |t|
      t.integer :angellist_market_id
      t.string :tag_type
      t.string :display_name
      t.string :angellist_url
    end

    create_table :locations do |t|
      t.integer :angellist_location_id
      t.string :tag_type
      t.string :display_name
      t.string :angellist_url
    end

    create_table :markets_startups do |t|
      t.integer :startup_id
      t.integer :market_id
    end

    create_table :locations_startups do |t|
      t.integer :startup_id
      t.integer :location_id
    end

    create_table :screenshots do |t|
      t.integer :startup_id
      t.string :thumb
      t.string :original
    end

    add_index :startups, :angellist_id
    add_index :markets, :angellist_market_id
    add_index :screenshots, :startup_id
    add_index :markets_startups, :startup_id
    add_index :markets_startups, :market_id
    add_index :locations_startups, :startup_id
    add_index :locations_startups, :location_id
  end
end
