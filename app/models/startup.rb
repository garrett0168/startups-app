class Startup < ActiveRecord::Base
  validates :angellist_id, :domain, :presence => true
  validates_uniqueness_of :angellist_id

  has_and_belongs_to_many :markets
  has_and_belongs_to_many :locations
  has_many :screenshots

  def self.search_by_domain(api, domain)
    api::search_by_domain(domain) do |code, body|
      if code == 200
        body = JSON.parse(body)
        if body["id"]
          startup = Startup.where(angellist_id: body["id"]).first
          return startup if startup

          opts = {domain: domain, angellist_id: body["id"], angellist_url: body["angellist_url"], name: body["name"],
                  angellist_url: body["angellist_url"], logo_url: body["logo_url"], description: body["product_desc"],
                  high_concept: body["high_concept"], thumb_url: body["thumb_url"], company_url: body["company_url"],
                  crunchbase_url: body["crunchbase_url"], blog_url: body["blog_url"], video_url: body["video_url"],
                  twitter_url: body["twitter_url"], quality: body["quality"], follower_count: body["follower_count"]}
          opts.merge!(status: body["status"]["message"]) if body["status"]
          startup = Startup.create!(opts)

          # create markets
          if body["markets"] && body["markets"].length > 0
            body["markets"].each do |market|
              marketObj = Market.where(angellist_market_id: market["id"]).first
              if marketObj
                startup.markets << marketObj
              else
                startup.markets.create!(angellist_market_id: market["id"], tag_type: market["tag_type"],
                  display_name: market["display_name"], angellist_url: market["angellist_url"])
              end
            end
          end
          # create screenshots
          if body["screenshots"] && body["screenshots"].length > 0
            body["screenshots"].each do |sc|
              startup.screenshots.create!(thumb: sc["thumb"], original: sc["original"])
            end
          end

          # create locations
          if body["locations"] && body["locations"].length > 0
            body["locations"].each do |location|
              locationObj = Location.where(angellist_location_id: location["id"]).first
              if locationObj
                startup.locations << locationObj
              else
                startup.locations.create!(angellist_location_id: location["id"], tag_type: location["tag_type"],
                  display_name: location["display_name"], angellist_url: location["angellist_url"])
              end
            end
          end

          return startup
        else
          Rails.logger.warn "Unexpected JSON in API response: #{body.inspect}"
          return nil
        end
      else
        Rails.logger.error "Encountered error while querying API for domain #{domain}. Response code: #{code}"
        return nil
      end
    end
  end
end
