startupsApp.controller('StartupsSearchController', ['$scope', '$location', '$http', function($scope, $location, $http) {
  $scope.domain = "";

  $scope.submitDomain = function()
  {
    $location.path("/startups/by_domain/" + $scope.domain);
  };
}]);

startupsApp.controller('StartupsByDomainController', ['$scope', '$routeParams', 'Startup', function($scope, $routeParams, Startup) {
  $scope.carouselGridWidth = 5;
  $scope.loading = true;
  $scope.startup = Startup.get({domain: $routeParams.domain}, 
    function(resource) 
    {
      $scope.loading = false;
    }, 
    function(response) 
    {
      $scope.loading = false;
      $scope.startup = null;
    });
}]);

startupsApp.controller('StartupsIndexController', ['$scope', 'Startups', function($scope, Startups) {
  $scope.totalStartups = 0;
  $scope.currentPage = 1;
  $scope.startupsPerPage = 5;
  $scope.startups = Startups.query(function() {
    $scope.totalStartups = $scope.startups.length;
  });
  $scope.startupsFiltered = [];

  $scope.$watch('currentPage', function() {
    $scope.filter();
  });

  $scope.$watch('totalStartups', function() {
    $scope.filter();
  });

  $scope.filter = function()
  {
    var offset = ($scope.currentPage - 1) * $scope.startupsPerPage;
    $scope.startupsFiltered = $scope.startups.slice( offset, offset+$scope.startupsPerPage );
  };
}]);
