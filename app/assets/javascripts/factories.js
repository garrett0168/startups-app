'use strict';

startupsApp.factory('Startups', ['$resource', function($resource) {
  return $resource('/startups');
}]);

startupsApp.factory('Startup', ['$resource', function($resource) {
  return $resource('/startups/by_domain/:domain');
}]);

