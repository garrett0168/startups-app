startupsApp.filter('formatLocations', function() {
  return function(locations)
  {
    if(locations && locations.length > 0)
    {
      return _.pluck(locations, 'display_name').join(", ");
    }
    else
    {
      return "";
    }
  };
});
