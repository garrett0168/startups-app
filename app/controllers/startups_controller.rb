class StartupsController < ApplicationController

  def index
    @startups = Startup.all
    respond_to do |format|
      format.json { render json: @startups.to_json }
    end
  end

  def by_domain
    @domain = params[:domain]
    begin
      @startup = Startup.search_by_domain(AngelListAPI, @domain)

    rescue => e
      Rails.logger.error "Unable to find startup by domain. Reason: #{e.message}"
      flash[:error] = "Unable to find startup by domain. Reason: #{e.message}"
      @startup = nil
    end

    respond_to do |format|
      format.json { render json: @startup.to_json(:include => [:markets, :screenshots, :locations]), status: @startup.nil? ? 404 : 200 }
    end
    
  end

end
