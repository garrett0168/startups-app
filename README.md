# Startups Lookup Web App

This is a web app for looking up startups by their domain. You can also view a list of saved startups.

Prerequisites
=============

* ruby 1.9.3
* PostgreSQL

For testing:

* [PhantomJS (1.9.0 or above)](http://phantomjs.org/download.html) Extract and put bin/phantomjs in your path.
* [Karma](http://karma-runner.github.io)

Setup
=====

1. `bundle install`
2. `bundle exec rake db:create`
3. `bundle exec rake db:migrate`

Running Tests
=============

Rspec tests:

`bundle exec rspec`

Cucumber tests:

`bundle exec cucumber`

Javascript tests:

`karma start spec/karma/config/dev.js`

Starting the server
===================

`bundle exec rails server`
