require 'httparty'

module AngelListAPI
  include HTTParty
  headers 'Content-TYpe'=>'application/json'
  base_uri 'http://api.angel.co/1'

  def self.search_by_domain(domain, &block)
    options = 
    { 
      :query => 
      { 
        :domain => domain
      }
    }
    resp = get("/startups/search.json", options)
    yield resp.code, resp.body
  end

end
