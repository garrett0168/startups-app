'use strict';

/* jasmine specs for controllers go here */
describe('Startups controllers', function() {
  var $scope = null;
  var $controller = null;
  var $httpBackend = null;
  var $location = null;

  beforeEach(function() {
    this.addMatchers({
      toEqualData: function(expect) {
        return angular.equals(expect, this.actual);
      }
    });
  });

  beforeEach(module('startupsApp'));

  beforeEach(inject(function($rootScope, _$controller_, _$httpBackend_, _$location_) {
    $scope = $rootScope.$new();
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    $location = _$location_;
  }));

  describe('StartupsIndexController', function() {

    it('should query for some startups', function() {
      // Expect that the resource (or http) makes a request.
      $httpBackend.expect('GET', '/startups').respond([{id:1, domain:'visible.vc', angellist_id:1, name:'Visible', angellist_url: ''}, {id:2, domain:'start.me', angellist_id:3, name:'Start', angellist_url: ''}]);
    
      var ctrl = $controller('StartupsIndexController', {$scope: $scope});
    
      expect($scope.startups).toEqual([]);
    
      // Simulate server response.
      $httpBackend.flush();
    
      expect($scope.startups).toEqualData([{id:1, domain:'visible.vc', angellist_id:1, name:'Visible', angellist_url: ''}, {id:2, domain:'start.me', angellist_id:3, name:'Start', angellist_url: ''}]);
      expect($scope.totalStartups).toEqual(2);
    });

    it('should handle pagination locally', function() {
      $httpBackend.expect('GET', '/startups').respond([{id:1, domain:'visible.vc', angellist_id:1, name:'Visible', angellist_url: ''}, {id:2, domain:'start.me', angellist_id:3, name:'Start', angellist_url: ''}, {id:3, domain:'placeholder1.me', angellist_id:5, name:'Placeholder', angellist_url: ''}, {id:4, domain:'placeholder2.me', angellist_id:123, name:'Placeholder', angellist_url: ''}, {id:5, domain:'placeholder3.me', angellist_id:9, name:'Placeholder', angellist_url: ''}, {id:6, domain:'placeholder4.me', angellist_id:678, name:'Placeholder', angellist_url: ''}, {id:7, domain:'placeholder5.me', angellist_id:8, name:'Placeholder', angellist_url: ''}, {id:8, domain:'placeholder6.me', angellist_id:987, name:'Placeholder', angellist_url: ''}]);
    
      var ctrl = $controller('StartupsIndexController', {$scope: $scope});
    
      // Simulate server response.
      $httpBackend.flush();

      // First page
      $scope.filter();
      expect($scope.startupsFiltered).toEqualData([{id:1, domain:'visible.vc', angellist_id:1, name:'Visible', angellist_url: ''}, {id:2, domain:'start.me', angellist_id:3, name:'Start', angellist_url: ''}, {id:3, domain:'placeholder1.me', angellist_id:5, name:'Placeholder', angellist_url: ''}, {id:4, domain:'placeholder2.me', angellist_id:123, name:'Placeholder', angellist_url: ''}, {id:5, domain:'placeholder3.me', angellist_id:9, name:'Placeholder', angellist_url: ''}]);

      // Next page
      $scope.currentPage = 2;
      $scope.filter();
      expect($scope.startupsFiltered).toEqualData([{id:6, domain:'placeholder4.me', angellist_id:678, name:'Placeholder', angellist_url: ''}, {id:7, domain:'placeholder5.me', angellist_id:8, name:'Placeholder', angellist_url: ''}, {id:8, domain:'placeholder6.me', angellist_id:987, name:'Placeholder', angellist_url: ''}]);
    });
  });

  describe('StartupsSearchController', function() {
    it('has an empty domain by default', function() {
      var ctrl = $controller('StartupsSearchController', {$scope: $scope});
      expect($scope.domain).toEqual('');
    });

    it('can search a domain', function() {
      var ctrl = $controller('StartupsSearchController', {$scope: $scope});
      $scope.domain = "visible.vc";

      $scope.submitDomain();

      expect($location.path()).toBe('/startups/by_domain/visible.vc');
    });
  });

  describe('StartupsByDomainController', function() {
    it('should automatically load the startup', function() {
      // Expect that the resource (or http) makes a request.
      $httpBackend.expect('GET', '/startups/by_domain/visible.vc').respond({id:1, domain:'visible.vc', angellist_id:1, name:'Visible', angellist_url: ''});
    
      var ctrl = $controller('StartupsByDomainController', {$scope: $scope, $routeParams: {domain: 'visible.vc'}});
    
      // Simulate server response.
      $httpBackend.flush();
    
      expect($scope.startup).toEqualData({id:1, domain:'visible.vc', angellist_id:1, name:'Visible', angellist_url: ''});
    });

    it('should track whether it is loading the resource from the server', function() {
      $httpBackend.expect('GET', '/startups/by_domain/visible.vc').respond({id:1, domain:'visible.vc', angellist_id:1, name:'Visible', angellist_url: ''});
      var ctrl = $controller('StartupsByDomainController', {$scope: $scope, $routeParams: {domain: 'visible.vc'}});

      expect($scope.loading).toEqual(true);

      $httpBackend.flush();

      expect($scope.loading).toEqual(false);
    });
  });
});
