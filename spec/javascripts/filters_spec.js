'use strict';

describe('filter', function() {
  beforeEach(module('startupsApp'));

  describe('formatLocations', function() {
    it('should format a location array into a string',
        inject(function(formatLocationsFilter) {
      expect(formatLocationsFilter(null)).toBe('');
      expect(formatLocationsFilter([])).toBe('');
      expect(formatLocationsFilter([{display_name: 'Chicago'}])).toBe('Chicago');
      expect(formatLocationsFilter([{display_name: 'Chicago'}, {display_name: 'San Francisco'}])).toBe('Chicago, San Francisco');
      expect(formatLocationsFilter([{display_name: 'Chicago'}, {display_name: 'San Francisco'}, {display_name: 'New York'}])).toBe('Chicago, San Francisco, New York');
    }));
  });
});
