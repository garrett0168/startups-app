require 'spec_helper'

module AngelListMockAPI
  module Visible
    def self.search_by_domain(domain, &block)
      json = File.read("#{Rails.root}/test/fixtures/visible.json")
      yield 200, json
    end
  end

  module NotFound
    def self.search_by_domain(domain, &block)
      json = File.read("#{Rails.root}/test/fixtures/not_found.json")
      yield 404, json
    end
  end

  module BadRequest
    def self.search_by_domain(domain, &block)
      json = File.read("#{Rails.root}/test/fixtures/bad_request.json")
      yield 400, json
    end
  end
end

describe Startup do
  it "is not valid without an Angel List id" do
    startup = Startup.new
    startup.should have(1).error_on(:angellist_id)
  end

  it "is not valid without a domain" do
    startup = Startup.new
    startup.should have(1).error_on(:domain)
  end

  it "is must have a unique Angel List id" do
    startup = FactoryGirl.create(:startup)

    startup2 = Startup.new(angellist_id: startup.angellist_id)
    startup2.should have(1).error_on(:angellist_id)

    startup2.angellist_id = "82745"
    startup2.should have(0).errors_on(:angellist_id)
  end

  describe "search by domain" do
    before(:each) do
      @domain = "visible.vc"
    end

    it "can search by domain" do
      startup = Startup.search_by_domain(AngelListMockAPI::Visible, @domain)

      startup.should_not be_nil
      startup.angellist_id.should == 82745
      startup.name.should == "Visible"
      startup.logo_url.should == "https://s3.amazonaws.com/photos.angel.co/startups/i/82745-5bb8026358bafed53fa639e2cac90f77-medium_jpg.jpg?buster=1357598262"
      startup.thumb_url.should == "https://s3.amazonaws.com/photos.angel.co/startups/i/82745-5bb8026358bafed53fa639e2cac90f77-thumb_jpg.jpg?buster=1357598262"
      startup.quality.should == 6
      startup.description.should == "Visible simplifies the sharing, management, and reporting of data between startups and investors. Visible was launched by a group of founders and investors that had been struggling with the vexing, but all too common, problems of tracking investment performance and managing communications between startups and investors. "
      startup.high_concept.should == "Data sharing for startups and their investors."
      startup.company_url.should == "http://www.visible.vc"
      startup.follower_count.should == 76
      startup.crunchbase_url.should be_nil
      startup.twitter_url.should == "http://twitter.com/visiblevc"
      startup.video_url.should be_nil
      startup.blog_url.should == ""
    end

    it "can create markets while searching" do
      startup = Startup.search_by_domain(AngelListMockAPI::Visible, @domain)

      startup.should_not be_nil
      startup.markets.length.should == 4

      market = startup.markets.last
      market.angellist_market_id.should == 15805
      market.tag_type.should == "MarketTag"
      market.display_name.should == "Angels"
      market.angellist_url.should == "https://angel.co/angel-investing"
    end

    it "won't create duplicate markets" do
      market = Market.create!(angellist_market_id: 15805, tag_type: "MarketTag", display_name: "Angels", angellist_url: "https://angel.co/angel-investing")

      startup = Startup.search_by_domain(AngelListMockAPI::Visible, @domain)

      startup.should_not be_nil
      startup.markets.length.should == 4

      Market.all.length.should == 4
    end

    it "can create screenshots" do
      startup = Startup.search_by_domain(AngelListMockAPI::Visible, @domain)

      startup.should_not be_nil
      startup.screenshots.length.should == 3

      sc = startup.screenshots.last
      sc.thumb.should == "https://s3.amazonaws.com/screenshots.angel.co/9d/82745/c9ab52c4ccb7a68d3a6a5082c2dbff91-thumb_jpg.jpg"
      sc.original.should == "https://s3.amazonaws.com/screenshots.angel.co/9d/82745/c9ab52c4ccb7a68d3a6a5082c2dbff91-original.png"
    end

    it "can create locations" do
      startup = Startup.search_by_domain(AngelListMockAPI::Visible, @domain)

      startup.should_not be_nil
      startup.locations.length.should == 1

      location = startup.locations.last
      location.angellist_location_id.should == 1648
      location.tag_type.should == "LocationTag"
      location.display_name.should == "Indianapolis"
      location.angellist_url.should == "https://angel.co/indianapolis"
    end

    it "won't duplicate locations" do
      location = Location.create!(angellist_location_id: 1648, tag_type: "LocationTag", display_name: "Indianapolis", angellist_url: "https://angel.co/indianapolis")

      startup = Startup.search_by_domain(AngelListMockAPI::Visible, @domain)

      startup.should_not be_nil
      startup.locations.length.should == 1

      Location.all.length.should == 1
    end

    it "can handle errors gracefully" do
      startup = Startup.search_by_domain(AngelListMockAPI::NotFound, @domain)
      startup.should be_nil

      startup = Startup.search_by_domain(AngelListMockAPI::BadRequest, @domain)
      startup.should be_nil
    end
  end
end
