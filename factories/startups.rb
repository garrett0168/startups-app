FactoryGirl.define do
  factory :startup do
    sequence(:angellist_id) {|n| "#{n.to_s*4}"} 
    name "My startup"
    domain "startup.me"
    description "My cool new startup"
    high_concept "a startup for startups"
    angellist_url "http://www.startup.com"
    logo_url "http://www.google.com"
    thumb_url "http://www.google.com"
    company_url "http://www.startup.com"
    crunchbase_url nil
    blog_url "http://blog.startup.com"
    video_url nil
    twitter_url "http://twitter.com/startup"
    status "status string"
    quality 1000
    follower_count 1000
  end
end
